<?php
require_once('functions.php');
echo custom_header('Version of instances');

require_once 'database/ini.php';
$db = new Database("sqlite",__DIR__."/database.db");

$versions = $db->select('SELECT version, count(*) AS ct, timestamp from instances where users > 0 GROUP BY version ORDER BY ct DESC, version DESC');
$sourceTime = $versions[0]->timestamp;

$countTotal = 0;
foreach ($versions as $key => $value) {
  $countTotal += $value->ct;
}

echo "Last refresh: ".date("d M Y G:i:s T",$sourceTime)." (refresh everyday)";
?>

<h1>Mastodon instances based on: <a href='https://instances.social/'>instances.social</a></h1>
<h2>Quick information</h2>
There are <strong><?= $countTotal ?></strong> instances<br>
Powered by <strong><?= count($versions) ?></strong> different versions</br></br>

<?php
foreach ($versions as $key => $row) {
  ( is_null($row->version) ) ? $version="Unknown" : $version=$row->version;

  echo '<a href="versions_list?version='.$version.'">'.$version.'</a> : <strong>'.$row->ct.'</strong> ('.round(100*$row->ct/$countTotal,2).'%)</br>';
}
?>

</div>

<?=custom_footer()?>

</body>
</html>
