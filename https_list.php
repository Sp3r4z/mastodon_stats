<?php
$rankSearch=(isset($_REQUEST['rank']))? $_REQUEST['rank'] : "ALL";
require_once('functions.php');
echo custom_header('HTTPS rank of instances - '.$rankSearch);

require_once 'database/ini.php';

if($rankSearch=='Unknown') $where = " AND https_rank IS NULL";
else if($rankSearch != 'ALL') $where = " AND https_rank = '$rankSearch'";
else $where = '';

$db = new Database("sqlite",__DIR__."/database.db");
$https = $db->select('SELECT https_rank as rank, name, timestamp from instances where users > 0'.$where.' ORDER BY name');
$sourceTime = $https[0]->timestamp;

echo "Last refresh: ".date("d M Y G:i:s T",$sourceTime)." (refresh everyday)";

?>
<h1>Mastodon instance HTTPS RANK: <?= $rankSearch ?></h1>
<h2>Quick information</h2>
<h3>There are <strong><?= count($https) ?></strong> instances in <?= $rankSearch ?><br></h3>
<?php foreach ($https as $key => $row):
  echo "<a href='https://".$row->name."/'>".$row->name."</a></br>";
endforeach; ?>
</div>
<?=custom_footer()?>
</body>
</html>
