<?php
require_once('functions.php');
echo custom_header('Mastodon Stats on all datas');
// error_reporting(0);
require 'database/ini.php';
function getValues($db,$date) {
  $sql='SELECT users, statuses, connections, statuses/statuses as TpU FROM mastodon where date=? AND statuses > 0 AND connections > 0 AND statuses > 0';
  $params=[$date];
  return $db->select($sql,$params);
}
function formatNumber($nb) {
  return number_format($nb, 0, ',', ' ');
}
?>
<h1>Statistics based on: <a href='https://instances.social/'>instances.social</a></h1>
<?php
$db=new Database($db_type,$db_host,$db_name,$db_user,$db_pwd);
$sql='SELECT date FROM mastodon GROUP BY date ORDER BY date DESC';
$dates=$db->select($sql,NULL);
$ctDates=count($dates);
$newest=getValues($db,$dates[0]->date);
$oldest=getValues($db,$dates[$ctDates-1]->date);
foreach ($newest as $key => $row) {
  $users_N[$key] = $row->users;
  $statuses_N[$key] = $row->statuses;
  $connections_N[$key] = $row->connections;
}
foreach ($oldest as $key => $row) {
  $users_O[$key] = $row->users;
  $statuses_O[$key] = $row->statuses;
  $connections_O[$key] = $row->connections;
}
?>
<h2>Some interesting informations</h2>
<h3>About users</h3>
<table class='table table-striped'>
  <thead>
    <th></th>
    <th><?=date("Y-m-d",$dates[0]->date)?></th>
    <th><?=date("Y-m-d",$dates[$ctDates-1]->date)?></th>
    <th>Evolution</th>
  </thead>
  <tbody>
    <tr>
      <td>Instances</td>
      <?
      $new = count($users_N);
      $old = count($users_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Total</td>
      <?
      $new = array_sum($users_N);
      $old = array_sum($users_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Max on instance</td>
      <?
      $new = max($users_N);
      $old = max($users_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Average/instance</td>
      <?
      $new = array_sum($users_N)/count($users_N);
      $old = array_sum($users_O)/count($users_O);
      ?>
      <td><?=formatNumber(round($new))?></td>
      <td><?=formatNumber(round($old))?></td>
      <td><?=formatNumber(round($new-$old))?></td>
    </tr>
    <tr>
      <td>Median/instance</td>
      <?
      $new = medianValue($users_N);
      $old = medianValue($users_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new - $old)?></td>
    </tr>
    <tr>
      <td>1st quartile</td>
      <?
      $new = percentile($users_N,25);
      $old= percentile($users_O,25);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>4th quartile</td>
      <?
      $new = percentile($users_N,75);
      $old= percentile($users_O,75);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Standard deviation</td>
      <?
      $new = ecarttype($users_N);
      $old= ecarttype($users_O);
      ?>
      <td><?=formatNumber(round($new))?></td>
      <td><?=formatNumber(round($old))?></td>
      <td><?=formatNumber(round($new-$old))?></td>
    </tr>
    <tr>
      <td>Shannon's diversity index</td>
      <?
      $new = entropy($users_N);
      $old= entropy($users_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Gini index</td>
      <?
      $new = gini($users_N);
      $old= gini($users_O);
      ?>
      <td><?=round($new,4)*100?>%</td>
      <td><?=round($old,4)*100?>%</td>
      <td><?=round($new-$old,4)*100?>%</td>
    </tr>
  </tbody>
</table>
<h3>About statuses</h3>
<table class='table table-striped'>
  <thead>
    <th></th>
    <th><?=date("Y-m-d",$dates[0]->date)?></th>
    <th><?=date("Y-m-d",$dates[$ctDates-1]->date)?></th>
    <th>Evolution</th>
  </thead>
  <tbody>
    <tr>
      <td>Total</td>
      <?
      $new = array_sum($statuses_N);
      $old = array_sum($statuses_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Max on instance</td>
      <?
      $new = max($statuses_N);
      $old = max($statuses_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Average/instance</td>
      <?
      $new = array_sum($statuses_N)/count($statuses_N);
      $old = array_sum($statuses_O)/count($statuses_O);
      ?>
      <td><?=formatNumber(round($new))?></td>
      <td><?=formatNumber(round($old))?></td>
      <td><?=formatNumber(round($new-$old))?></td>
    </tr>
    <tr>
      <td>Median/instance</td>
      <?
      $new = medianValue($statuses_N);
      $old = medianValue($statuses_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new - $old)?></td>
    </tr>
    <tr>
      <td>1st quartile</td>
      <?
      $new = percentile($statuses_N,25);
      $old= percentile($statuses_O,25);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>4th quartile</td>
      <?
      $new = percentile($statuses_N,75);
      $old= percentile($statuses_O,75);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Standard deviation</td>
      <?
      $new = ecarttype($statuses_N);
      $old= ecarttype($statuses_O);
      ?>
      <td><?=formatNumber(round($new))?></td>
      <td><?=formatNumber(round($old))?></td>
      <td><?=formatNumber(round($new-$old))?></td>
    </tr>
    <tr>
      <td>Shannon's diversity index</td>
      <?
      $new = entropy($statuses_N);
      $old= entropy($statuses_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Gini index</td>
      <?
      $new = gini($statuses_N);
      $old= gini($statuses_O);
      ?>
      <td><?=round($new,4)*100?>%</td>
      <td><?=round($old,4)*100?>%</td>
      <td><?=round($new-$old,4)*100?>%</td>
    </tr>
  </tbody>
</table>
<h3>About connections</h3>
<table class='table table-striped'>
  <thead>
    <th></th>
    <th><?=date("Y-m-d",$dates[0]->date)?></th>
    <th><?=date("Y-m-d",$dates[$ctDates-1]->date)?></th>
    <th>Evolution</th>
  </thead>
  <tbody>
    <tr>
      <td>Max on instance</td>
      <?
      $new = max($connections_N);
      $old = max($connections_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Average/instance</td>
      <?
      $new = array_sum($connections_N)/count($connections_N);
      $old = array_sum($connections_O)/count($connections_O);
      ?>
      <td><?=formatNumber(round($new))?></td>
      <td><?=formatNumber(round($old))?></td>
      <td><?=formatNumber(round($new-$old))?></td>
    </tr>
    <tr>
      <td>Median/instance</td>
      <?
      $new = medianValue($connections_N);
      $old = medianValue($connections_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new - $old)?></td>
    </tr>
    <tr>
      <td>1st quartile</td>
      <?
      $new = percentile($connections_N,25);
      $old= percentile($connections_O,25);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>4th quartile</td>
      <?
      $new = percentile($connections_N,75);
      $old= percentile($connections_O,75);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Standard deviation</td>
      <?
      $new = ecarttype($connections_N);
      $old= ecarttype($connections_O);
      ?>
      <td><?=formatNumber(round($new))?></td>
      <td><?=formatNumber(round($old))?></td>
      <td><?=formatNumber(round($new-$old))?></td>
    </tr>
    <tr>
      <td>Shannon's diversity index</td>
      <?
      $new = entropy($connections_N);
      $old= entropy($connections_O);
      ?>
      <td><?=formatNumber($new)?></td>
      <td><?=formatNumber($old)?></td>
      <td><?=formatNumber($new-$old)?></td>
    </tr>
    <tr>
      <td>Gini index</td>
      <?
      $new = gini($connections_N);
      $old= gini($connections_O);
      ?>
      <td><?=round($new,4)*100?>%</td>
      <td><?=round($old,4)*100?>%</td>
      <td><?=round($new-$old,4)*100?>%</td>
    </tr>
  </tbody>
</table>
</div>
<?=custom_footer()?>
</body>
</html>
