<?php
require_once('functions.php');
echo custom_header('Instances not connected to fediverse');
?>
<h1>Mastodon instances list based on: <a href='https://instances.social/'>instances.social</a></h1>
<?php
require 'database/ini.php';
function getRank($s) {
  if($s<20) return 'F';
  else if($s<35) return 'E';
  else if($s<50) return 'D';
  else if($s<65) return 'C';
  else if($s<80) return 'B';
  else if($s<100) return 'A';
  else return 'A+';
}
$db=new Database($db_type,$db_host,$db_name,$db_user,$db_pwd);
$sql='SELECT distinct date FROM mastodon ORDER BY date DESC LIMIT 1';
$resultDate=$db->select($sql,[],true);
$cronTime=$resultDate->date;
echo "Last refresh: ".date("d M Y G:i:s T",$cronTime)." (refresh everyday at: 1:35am Europe/Paris)";
$sql="SELECT * FROM mastodon WHERE date=? AND connections = 0 ORDER BY users DESC";
$data=[$cronTime];
$instances=$db->select($sql,$data);

$db = new Database("sqlite",__DIR__."/database.db");
$names = array_column(json_decode(json_encode($instances),true),'name');
$list_names = (count($names)>1)? implode('", "',$names): $names[0];

$locations = $db->select('SELECT name, country FROM locations WHERE name IN ("'.$list_names.'")');

?>
<h2>Quick information</h2>
There are <strong><?= count($instances)?></strong> instances not connected to fediverse<br><br>
<table class='table table-striped sortable'>
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th><i class='icon-users' aria-hidden='true' title='users count'></i><span class='sr-only'>Number of users</span></th>
      <th><i class='icon-sticky-note-o' aria-hidden='true' title='toots count'></i><span class='sr-only'>Number of toots</span></th>
      <th><i class='icon-globe' aria-hidden='true' title='country'></i><span class='sr-only'>Country of instance</span></th>
      <th>HTTPS</th>
      <th>IPv6</th>
      <th>Open</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach ($locations as $keyL => $rowL) {
      $tableLocations[$rowL->name]=$rowL->country;
    }
    $i=0;
    foreach ($instances as $key => $row) {
      $i++;
      echo "<tr>
      <td>$i</td>";
      echo '<td><a href="profile?uri='.base64_encode($row->name).'">'.$row->name.'</a></td>
      <td data-value="'.$row->users.'">'.number_format($row->users,0,',',' ').'</td>
      <td data-value="'.$row->statuses.'">'.number_format($row->statuses,0,',',' ').'</td>';
      echo (isset($tableLocations[$row->name]))?'<td>'.$tableLocations[$row->name].'</td>':'<td>Unknown</td>';
      echo ($row->https_score>=80) ? "<td class='success' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>" : (($row->https_score>65) ? "<td class='warning' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>" : "<td class='danger' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>");
      echo ($row->ipv6)?"<td class='success'>YES</td>":"<td class='danger'>NO</td>";
      echo ($row->openRegistrations)?"<td class='success'>YES</td>":"<td class='danger'>NO</td>";
      echo '</tr>';
    }
    ?>
  </tbody>
</table>
</div>
<?=custom_footer()?>
</body>
</html>
