const { src, dest, parallel, series, watch } = require('gulp'),
postcss = require('gulp-postcss'),
cssnano = require('cssnano'),
sorting = require('postcss-sorting'),
autoprefixer = require('autoprefixer'),
uncss = require('postcss-uncss'),
uglify = require('gulp-uglify'),
sass = require('gulp-sass'),
gulpSequence = require('gulp-sequence'),
babel = require('gulp-babel'),
concat = require('gulp-concat'),
cssImport= require("gulp-cssimport"),
atImport = require("postcss-import-url")

sources = 'src/',
dist = 'assets/',

build_sass = () => {
	return src([sources+'sass/*.{sass,scss}'])
	.pipe(sass({
		outputStyle:'expanded',
		indentType:'tab',
		indentWidth:1
	}))
	.on('error', sass.logError)
	.pipe(dest(sources+'css/'))
},

watching = () => {
	watch(sources+'sass/**/*.{sass,scss}', series(build_sass,css_prod));
	watch(sources+'js/**/*.js', js_prod);
},

// PRODUCTION
css_prod = () => {
	let plugins = [
		atImport(),
		uncss({
			ignore: ['.addArrow','.dir-u','.dir-d','.btn-outline-dark','.btn-outline-light','.d-table','.d-none','td.warning','td.success','td.danger','.darkTheme'],
			html: ['http://localhost/mastodon/general.php'],
		}),
		sorting({
			'order': [
				'custom-properties',
				'dollar-variables',
				'declarations',
				'rules',
				'at-rules'
			],
			'properties-order': 'alphabetical'
		}),
		autoprefixer(),
		cssnano(),
	];
	return src(sources+'css/**/*.css')
	.pipe(cssImport())
	.pipe(postcss(plugins))
	.pipe(dest(dist+'css/'));
},

js_prod = () => {
	return src(sources+'js/**/*.js')
	.pipe(concat('script.js'))
	.pipe(babel({
		presets: ['@babel/env']
	}))
	.pipe(uglify())
	.on('error', function(err) {
		console.error('Error in JS compress task', err.toString());
	})
	.pipe(dest(dist+'js/'))
},

fonts_prod = () => {
	return src([sources+'fonts/**/*'])
	.pipe(dest(dist+'fonts/'))

}

exports.sass = build_sass
exports.css_prod = css_prod
exports.js_prod = js_prod
exports.prod = series(build_sass, parallel(js_prod,css_prod), fonts_prod)
exports.watch = watching
exports.default = watching
