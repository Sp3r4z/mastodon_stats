<?php
require_once('functions.php');
echo custom_header('Country of instances');

require_once 'database/ini.php';

$db = new Database("sqlite",__DIR__."/database.db");

$tableLocations = $db->select('SELECT country, timestamp, count(*) AS ct FROM locations GROUP BY country ORDER BY ct DESC');

$sourceTime = $tableLocations[0]->timestamp;
$countTotal = array_sum( array_column(json_decode(json_encode($tableLocations), true),'ct') );

?>

<h1>Mastodon instance based on: <a href='https://instances.social/'>instances.social</a></h1>
<h2>Countries provide by <a href="https://miaou.drycat.fr/@Dryusdan">@Dryusdan</a></h2>

Last refresh: <?=date("d M Y G:i:s T",$sourceTime)?> (refresh everyday)
<h2>Quick information</h2>
There are <strong><?= $countTotal ?></strong> instances<br>
Hosted in <strong><?= count($tableLocations) ?></strong> locations</br></br>
<?php
foreach ($tableLocations as $key => $row) {
  echo '<a href="countries_list?location='.$row->country.'">'.$row->country.'</a>: <strong>'.$row->ct.'</strong> ('.round(100*$row->ct/$countTotal,2).'%)</br>';
}
?>
</div>
<?=custom_footer()?>
</body>
</html>
