<?php
require_once 'functions.php';
$date1=date_create(date('Y-m-d'))->sub(new DateInterval('P6M'));
$date2=date_create(date('Y-m-d'));
$days=date_diff($date2,$date1)->format('%a');
tableInstances("last six months",$days);