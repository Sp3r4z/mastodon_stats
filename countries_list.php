<?php
$countrySearch=(isset($_REQUEST['location']))? $_REQUEST['location'] : "NOWHERE";
require_once('functions.php');
echo custom_header('Country of instances - '.$countrySearch);

require_once 'database/ini.php';

$db = new Database("sqlite",__DIR__."/database.db");

if($countrySearch=='Unknown') $where = "WHERE country = 'Unknown'";
else if($countrySearch != 'NOWHERE') $where = "WHERE  country = '$countrySearch'";
else $where = '';

$locations = $db->select('SELECT country, name, timestamp from locations '.$where.' ORDER BY name');

$sourceTime=date('d M Y G:i:s T',$locations[0]->timestamp);

?>

<h1>Mastodon instance hosted in: <?= $countrySearch ?></h1>
<h2>Hostings provide by <a href="https://miaou.drycat.fr/@Dryusdan">@Dryusdan</a></h2>
Last refresh: <?=$sourceTime?> (refresh everyday)
<h2>Quick information</h2>
<h3>There are <strong><?= count($locations) ?></strong> instances in <?= $countrySearch ?><br></h3>

<?php
foreach ($locations as $key => $row) {
  if($countrySearch!="NOWHERE" && $countrySearch==$row->country)
  echo "<a href='https://".$row->name."/'>".$row->name."</a></br>";
}
?>

</div>

<?=custom_footer()?>

</body>
</html>
