<?php
require_once('functions.php');
echo custom_header('Mastodon general stats');
// error_reporting(0);

require 'database/ini.php';
$db = new Database("sqlite",__DIR__."/database.db");

$table = $db->select('SELECT ins.*, loc.country, (statuses/users) AS tootsusers	FROM instances AS ins LEFT JOIN locations AS loc	ON loc.name=ins.name WHERE users > 0');
$sourceTime = $table[0]->timestamp;
$tableCount = count($table);

?>
<h1>Statistics based on: <a href='https://instances.social/'>instances.social</a></h1>
Last refresh: <?= date("d M Y G:i:s T",$sourceTime) ?>
<?php

$table = json_decode(json_encode($table), true);
$users = array_column( $table, 'users');
$usersCount=array_sum($users);

foreach ($table as $key => $row) {
	$usersPercent[$key] = round($row['users']*100/$usersCount,10);
}
rsort($usersPercent);
?>
<h2>Some interesting informations</h2>
There are <strong><?=number_format($usersCount, 0, ',', ' ' )?></strong> user inscriptions<br>
On <strong><?= number_format($tableCount,0,',',' ')?></strong> known instances<br>
Average: <strong><?=round($usersCount/$tableCount)?></strong> users per instance<br>
Median value: <strong><?=round(medianValue($users))?></strong> users per instance<br>
Maximum users on instance: <strong><?= number_format(max($users),0,',',' ')?></strong> users<br><br>
<?php
showPercentile(90, $users);
showPercentile(80, $users);
showPercentile(75, $users, true);
showPercentile(70, $users);
showPercentile(60, $users);
showPercentile(40, $users);
showPercentile(30, $users);
showPercentile(25, $users, true);
showPercentile(20, $users);
showPercentile(10, $users);
echo "<br>";
?>
Standard deviation value: <strong><?= number_format(round(ecarttype($users)),0,',',' ')?></strong> (more significant when divided by average: <strong><?= number_format(round(ecarttype($users)/($usersCount/$tableCount)),0,',',' ')?></strong>)<br>
<?php $ent=entropy($users); ?>
Shannon's diversity index, instances act like there are: <strong><?=$ent?></strong> (Shannon index)<br>
<?php $gini=gini($users);?>
Gini index: <strong><?=round($gini,2)?></strong> (<?=round($gini*100,2)?>% of centralisation) <br>
<?php
showPercents(3, $usersPercent);
showPercents(5, $usersPercent);
showPercents(10,$usersPercent);
showPercents(25,$usersPercent);
echo "<br>";
$statuses = array_column($table,'statuses');
$uptime = array_column($table,'uptime');
$ipv6 = array_column($table,'ipv6');
$open = array_column($table,'open');
?>
More than <strong><?= number_format( array_sum($statuses), 0, ',', ' ' ) ?></strong> posted toots<br>
Average per instance: <strong><?= number_format( array_sum($statuses)/$tableCount, 0, ',', ' ' ) ?></strong> toots<br>
Median value per instance: <strong><?= round(medianValue($statuses)) ?></strong> toots<br>
Average per user: <strong><?= round( array_sum($statuses)/$usersCount) ?></strong> toots<br><br>
Average uptime: <strong><?= round((array_sum($uptime)/count($uptime))*100,2) ?>%</strong><br>
Median uptime: <strong><?= round(medianValue($uptime)*100,2) ?>%</strong><br><br>
There are <strong><?= array_sum($ipv6) ?></strong> (<?= percent(array_sum($ipv6),$tableCount) ?>%) instances with IPv6 activated<br>
There are <strong><?= array_sum($open) ?></strong> (<?= percent(array_sum($open),$tableCount) ?>%) open registration instances and <strong><?= $tableCount-array_sum($open) ?></strong> (<?= 100-percent(array_sum($open),$tableCount) ?>%)  actually closed<br>

<?php

$nb_list=10;
echo "<h2 class='tableShow'>The ".$nb_list." biggest open registration instances</h2>";
$table = $db->select('SELECT ins.*, loc.country, (statuses/users) AS tootsusers FROM instances AS ins LEFT JOIN locations AS loc ON loc.name=ins.name WHERE users > 0 AND open=1 ORDER BY users DESC, https_score DESC LIMIT '.$nb_list);
showTable($table, $nb_list, "premier");

$nb_list=20;
$table = $db->select('SELECT ins.*, loc.country, (statuses/users) AS tootsusers FROM instances AS ins LEFT JOIN locations AS loc ON loc.name=ins.name WHERE users > 0 ORDER BY users DESC, https_score DESC LIMIT '.$nb_list);
echo "<h2 class='tableShow'>The ".$nb_list." biggest instances in number of users</h2>";
showTable($table, $nb_list, "troisieme");


$nb_list=20;
$table = $db->select('SELECT ins.*, loc.country, (statuses/users) AS tootsusers FROM instances AS ins LEFT JOIN locations AS loc ON loc.name=ins.name WHERE users > 0 AND open = 1 ORDER BY tootsusers DESC, users DESC LIMIT '.$nb_list);
echo "<h2 class='tableShow'>The ".$nb_list." biggest open registrations intances sorted by: toots/user ratio</h2>";
showTable($table, $nb_list, "quatrieme");

?>
</div>
<?=custom_footer()?>
</body>
</html>
