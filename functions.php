<?php

function custom_header($title){
  return '
  <!DOCTYPE html>
  <html lang="en">
  <head>
  <meta charset="UTF-8">
  <meta name="description" content="Statistics about Mastodon, instances and growth">
  <meta name="keywords" content="mastodon, stats, statistics, fediverse, floss, free software">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>'.$title.'</title>
  <link rel="icon" href="favicon.png">
  <link rel="stylesheet" href="assets/css/main.css">
  </head>
  <body>
  <div class="container">
  <nav id="navbar" class="navbar navbar-dark">
  <a href="index"><i class="icon-home h3" title="home icon"></i><span class="sr-only">Go to home</span></a>
  <span class="h1">Mastostats</span>
  <button class="btn btn-outline-light" id="darkTheme" type="button">Light theme</button>
  </nav>
  ';
}
function custom_footer(){
  return '
  <footer>Content under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA</a> license — <a href="https://mastodon.xyz/">Sp3r4z</a></footer>

  <script src="assets/js/script.js" async defer></script>
  ';
}

function tableInstances($title,$days) {
  echo custom_header('Evolution of instances — '.$title)
  ?>
  <h1>What is the <?= $title ?> evolution of instances? based on: <a href="https://instances.social/">instances.social</a></h1>

  <?php

  require 'database/ini.php';

  function getRank($s) {
    if($s<20) return 'F';
    else if($s<35) return 'E';
    else if($s<50) return 'D';
    else if($s<65) return 'C';
    else if($s<80) return 'B';
    else if($s<100) return 'A';
    else return 'A+';
  }

  $db=new Database($db_type,$db_host,$db_name,$db_user,$db_pwd);

  $sql='SELECT date FROM mastodon GROUP BY date ORDER BY date DESC';
  $date=$db->select($sql,NULL);
  $cronTime=$date[0]->date;
  $compareDate=$date[$days]->date;

  $sql="SELECT DISTINCT result.openRegistrations AS openRegistrations, result.name, result.users AS usersT, tmp.users AS usersY, result.statuses AS statusesT, tmp.statuses AS statusesY, result.connections AS connectionsT, tmp.connections AS connectionsY, result.https_score AS https_score, result.uptime AS uptime FROM mastodon AS result LEFT JOIN mastodon AS tmp ON tmp.name=result.name WHERE result.date = ? AND tmp.date = ? ORDER BY result.name ASC";
  $data=[$cronTime,$compareDate];
  $result=$db->select($sql,$data);

  // var_dump($result);

  $db = new Database("sqlite",__DIR__."/database.db");
  $names = array_column(json_decode(json_encode($result),true),'name');
  $list_names = (count($names)>1)? implode('", "',$names): '"'.$names[0].'"';
  $locations = $db->select('SELECT name, country FROM locations WHERE name IN ("'.$list_names.'")');

  foreach ($locations as $keyL => $rowL) {
    $tableLocations[$rowL->name]=$rowL->country;
  }

  foreach ($result as $key => $row) {
    $usersY[$key] = $row->usersY;
    $usersT[$key] = $row->usersT;
    $statusesT[$key] = $row->statusesT;
    $statusesY[$key] = $row->statusesY;
  }

  ?>

  Datas between: <?= date("d M Y G:i:s T",$compareDate) ?> &amp; <?= date("d M Y G:i:s T",$cronTime)?> <br><br>
  Only <strong><?= number_format(count($result),0,',',' ')?></strong> listed instances<br>
  <strong><?= number_format(array_sum($usersT),0,',',' ')?></strong> listed users<br>
  <strong><?= number_format(array_sum($statusesT),0,',',' ')?></strong> listed statuses<br><br>

  There are <?= number_format(array_sum($usersT)-array_sum($usersY),0,',',' ') ?> new users<br>
  <?php if($days>1) { ?> With an average of <?= number_format((array_sum($usersT)-array_sum($usersY))/$days,0,',',' ') ?> new users per day<br><?php } ?>

  There are <?= number_format(array_sum($statusesT)-array_sum($statusesY),0,',',' ') ?> new toots<br>
  <?php if($days>1) { ?> With an average of <?= number_format((array_sum($statusesT)-array_sum($statusesY))/$days,0,',',' ') ?> new toots per day<br><?php } ?><br><br>

</div>

<div class="container-fluid">

  <table class='table table-striped sortable'>
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>

        <th title="users"><i class="icon-users" aria-hidden='true' title="Users count"></i><span class='sr-only'>Number of users</span></th>
        <th>↑</th>
        <th>%</th>

        <th title="toots"><i class="icon-sticky-note-o" aria-hidden='true' title="Toots count"></i><span class='sr-only'>Number of toots</span></th>
        <th>↑</th>
        <th>%</th>

        <th title="toots per user"><i class="icon-sticky-note-o" aria-hidden='true' title="Toots count"></i> / <i class="icon-user" aria-hidden='true' title="per user"></i><span class='sr-only'>Number of toots per user</span></th>
        <th>↑</th>
        <th>%</th>

        <th title="connections"><i class="icon-link" aria-hidden='true' title="Connections count"></i><span class='sr-only'>Number of connections</span></th>
        <th>↑</th>
        <th>%</th>

        <th title="country"><i class="icon-globe" aria-hidden='true' title="Country"></i><span class='sr-only'>Country of instance</span></th>

        <th>HTTPS</th>
        <th>Uptime</th>
        <th>Open</th>
      </tr>
    </thead>

    <tbody>
      <?php

      $countrySearch=(isset($_REQUEST['location']))? $_REQUEST['location'] : "ALL";

      $i=0;
      foreach ($result as $key => $row) {

        if( (isset($tableLocations[$row->name]) && $tableLocations[$row->name]==$countrySearch ) || $countrySearch=="ALL") {

          $i++;
          echo '<tr>
          <td>'.$i.'</td>

          <td><a href="profile?uri='.base64_encode($row->name).'">'.$row->name.'</a></td>';

          echo "<td data-value='".$row->usersT."'>".number_format($row->usersT,0,',',' ')."</td>";
          $usersPlus=$row->usersT-$row->usersY;
          echo ($usersPlus>0)?"<td class='success' data-value='".$usersPlus."'>+".number_format($usersPlus,0,',',' ')."</td>":(($usersPlus==0)?"<td class='warning' data-value='".$usersPlus."'>".number_format($usersPlus,0,',',' ')."</td>":"<td class='danger' data-value='".$usersPlus."'>".number_format($usersPlus,0,',',' ')."</td>");
          $userPercent=($usersPlus==0)?0:(($row->usersY==0)?$usersPlus*100:round($usersPlus*100/$row->usersY,2));
          echo ($userPercent>0)?"<td class='success' data-value='".$userPercent."'>+".number_format($userPercent,0,',',' ')."%</td>":(($userPercent==0)?"<td class='warning' data-value='".$userPercent."'>".number_format($userPercent,0,',',' ')."%</td>":"<td class='danger' data-value='".$userPercent."'>".number_format($userPercent,0,',',' ')."%</td>");

          echo '<td data-value="'.$row->statusesT.'">'.number_format($row->statusesT,0,',',' ').'</td>';
          $statusPlus=$row->statusesT-$row->statusesY;
          echo ($statusPlus>0)?"<td class='success' data-value='".$statusPlus."'>+".number_format($statusPlus,0,',',' ')."</td>":(($statusPlus==0)?"<td class='warning' data-value='".$statusPlus."'>".number_format($statusPlus,0,',',' ')."</td>":"<td class='danger' data-value='".$statusPlus."'>".number_format($statusPlus,0,',',' ')."</td>");
          $statusPercent=($statusPlus==0)?0:(($row->statusesY==0)?$statusPlus*100:round($statusPlus*100/$row->statusesY,2));
          echo ($statusPercent>0)?"<td class='success' data-value='".$statusPercent."'>+".number_format($statusPercent,0,',',' ')."%</td>":(($statusPercent==0)?"<td data-value='".$statusPercent."' class='warning'>".number_format($statusPercent,0,',',' ')."%</td>":"<td data-value='".$statusPercent."' class='danger'>".number_format($statusPercent,0,',',' ')."%</td>");

          $tuT=($row->statusesT)?round($row->statusesT/$row->usersT):0;
          echo '<td data-value="'.$tuT.'">'.number_format($tuT,0,',',' ').'</td>';
          $tuY=($row->statusesY)?round($row->statusesY/$row->usersY):0;
          $tuDiff=$tuT-$tuY;
          echo ($tuDiff>0)?'<td class="success" data-value="'.$tuDiff.'">+'.$tuDiff.'</td>':(($tuDiff==0)?'<td class="warning" data-value="'.$tuDiff.'">'.$tuDiff.'</td>':'<td class="danger" data-value="'.$tuDiff.'">'.$tuDiff.'</td>');
          $tuPercent=($tuDiff==0)?0:(($tuY==0)?$tuDiff*100:round($tuDiff*100/$tuY,2));
          echo ($tuPercent>0)?"<td class='success' data-value='".$tuPercent."'>+".number_format($tuPercent,0,',',' ')."%</td>":(($tuPercent==0)?"<td data-value='".$tuPercent."' class='warning'>".number_format($tuPercent,0,',',' ')."%</td>":"<td data-value='".$tuPercent."' class='danger'>".number_format($tuPercent,0,',',' ')."%</td>");

          echo '<td data-value="'.$row->connectionsT.'">'.number_format($row->connectionsT,0,',',' ').'</td>';
          $linksPlus=$row->connectionsT-$row->connectionsY;
          echo ($linksPlus>0)?"<td class='success' data-value='".$linksPlus."'>+".number_format($linksPlus,0,',',' ')."</td>":(($linksPlus==0)?"<td class='warning' data-value='".$linksPlus."'>".number_format($linksPlus,0,',',' ')."</td>":"<td class='danger' data-value='".$linksPlus."'>".number_format($linksPlus,0,',',' ')."</td>");
          $linksPercent=($linksPlus==0)?0:(($row->connectionsY==0)?$linksPlus*100:round($linksPlus*100/$row->connectionsY,2));
          echo ($linksPercent>0)?"<td class='success' data-value='".$linksPercent."'>+".number_format($linksPercent,0,',',' ')."%</td>":(($linksPercent==0)?"<td data-value='".$linksPercent."' class='warning'>".number_format($linksPercent,0,',',' ')."%</td>":"<td data-value='".$linksPercent."' class='danger'>".number_format($linksPercent,0,',',' ')."%</td>");

          echo (isset($tableLocations[$row->name]))?'<td>'.$tableLocations[$row->name].'</td>':'<td>Unknown</td>';

          echo ($row->https_score>=80) ? "<td class='success' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>" : (($row->https_score>65) ? "<td class='warning' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>" : "<td class='danger' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>");

          $finalUptime=$row->uptime*100;
          echo ($finalUptime>=90) ? "<td class='success' data-value='".$finalUptime."'>".round($finalUptime,3)."%</td>" : (($finalUptime>75) ? "<td class='warning' data-value='".$finalUptime."'>".round($finalUptime,3)."%</td>" : "<td class='danger' data-value='".$finalUptime."'>".round($finalUptime,3)."%</td>");

          echo ($row->openRegistrations)?"<td class='success'>YES</td>":"<td class='danger'>NO</td>";
          echo '</tr>';
        }

      }
      ?>

    </tbody>
  </table>
</div>

<?=custom_footer()?>

</body>
</html>
<?php
}

function medianValue($arr) {
  sort($arr);
  $count = count($arr); //total numbers in array
  $middleval = floor(($count-1)/2); // find the middle value, or the lowest middle value
  if($count % 2) { // odd number, middle is the median
    $median = $arr[$middleval];
  } else { // even number, calculate avg of 2 medians
    $low = $arr[$middleval];
    $high = $arr[$middleval+1];
    $median = (($low+$high)/2);
  }
  return $median;
}

function ecarttype($datas, $nbdecimals=0) {

  if (is_array($datas))	{
    reset($datas);
    $somme=0;
    $nbelement=count($datas);
    foreach ($datas as $value) {
      $somme += floatval($value);
    }
    $moyenne = $somme/$nbelement;
    reset($datas);
    $sigma=0;
    foreach ($datas as $value) {
      $sigma += pow((floatval($value)-$moyenne),2);
    }
    return sqrt($sigma/$nbelement);
  }
  else {
    return false;
  }
}

function showTable($table, $nb_list,$id) {
  echo "<table class='table table-striped table-perso sortable' id='".$id."'>
  <thead>
  <tr>";
  echo ($nb_list>10)?'<th scope="col">#</th>':'';

  echo "<th scope='col'>URL</th>
  <th scope='col'><i class='icon-users' aria-hidden='true' title='users count'></i><span class='sr-only'>Number of users</span></th>
  <th scope='col'><i class='icon-sticky-note-o' aria-hidden='true' title='toots count'></i><span class='sr-only'>Number of toots</span></th>
  <th scope='col'><i class='icon-sticky-note-o' aria-hidden='true' title='toots count'></i> / <i class='icon-user' aria-hidden='true' title='by user'></i><span class='sr-only'>Toots per user</span></th>
  <th scope='col'><i class='icon-link' aria-hidden='true' title='connections count'></i><span class='sr-only'>Number of connections</span></th>
  <th scope='col'><i class='icon-globe' aria-hidden='true' title='country'></i><span class='sr-only'>Country of instance</span></th>
  <th scope='col'>HTTPS</th>
  <th scope='col'>Uptime</th>
  <th scope='col'>IPv6</th>
  </tr>
  </thead>
  <tbody>";

  $i=0;
  foreach ($table as $key => $row) {
    if($row->users>0) {
      $i++;
      echo '<tr>';
      echo ($nb_list>10)?'<td>'.$i.'</td>':'';

      echo '<td><a href="profile?uri='.base64_encode($row->name).'">'.$row->name.'</a></td>
      <td data-value="'.$row->users.'">'.number_format($row->users,0,',',' ').'</td>
      <td data-value="'.$row->statuses.'">'.number_format($row->statuses,0,',',' ').'</td>
      <td>'.$row->tootsusers.'</td>
      <td>'.$row->connections.'</td>';

      echo (isset($row->country))?'<td>'.$row->country.'</td>':'<td>Unknown</td>';

      echo ($row->https_score>=80) ? "<td class='success' data-value='".$row->https_score."'>".$row->https_rank."</td>" :
      (($row->https_score>65) ? "<td class='warning' data-value='".$row->https_score."'>".$row->https_rank."</td>" :
      ( ( !isset($row->https_score) || !isset($row->https_rank) )? "<td class='danger' data-value='unknown'>Unknown</td>":
      '<td class="danger" data-value="'.$row->https_score.'">'.$row->https_rank.'</td>'));
      $uptime=$row->uptime*100;
      echo ($uptime>=99) ? "<td class='success' data-value='".$uptime."'>".round($uptime,2)."%</td>" : (($uptime>97) ? "<td class='warning' data-value='".$uptime."'>".round($uptime,2)."%</td>" : "<td class='danger' data-value='".$uptime."'>".round($uptime,2)."%</td>");
      echo ($row->ipv6)?"<td class='success'>YES</td>":"<td class='danger'>NO</td>";
      echo '</tr>';

      if($i==$nb_list) break;
    }
  }
  echo "</tbody></table>";
}

function percentile($datas, $percentile) {
  sort($datas);
  $index = ($percentile/100) * count($datas);
  if (floor($index) == $index) {
    $result = ($datas[$index-1] + $datas[$index])/2;
  }
  else {
    $result = $datas[floor($index)];
  }
  return $result;
}

function showPercentile($nb,$array,$quart=false) {
  $name='quartile';
  if(!$quart) {
    $name='decile';
    $percentile=$nb/10;
    $percentile.=($percentile===1)?'st':(($percentile===2)?'nd':(($percentile===3)?'rd':'th'));
  }
  else {
    ($nb==25)?$percentile='1st':$percentile='3rd';
  }
  echo "$percentile $name value: <strong>".number_format(round(percentile($array,$nb)),0,',',' ').'</strong> users<br>';
}

function showPercents($many,$datas) {
  $percent=0;
  for($i=0;$i<$many;$i++) {
    $percent+=$datas[$i];
  }
  $rp=round($percent,2);
  echo "The $many biggest instances representing <strong>$rp%</strong> of total users<br>";
}

// Shannon index
function entropy($datas) {
  $somme=array_sum($datas);
  $res=0;
  foreach( $datas as $nb => $nb_user){
    if($nb_user)
    $res += -floatval($nb_user) * log(floatval($nb_user),2);
  }
  $res = $res/$somme+log($somme,2) ;
  return round(pow(2,$res));
}

// Gini index
function gini($datas) {
  sort($datas);
  $ctTable=count($datas);
  $somme=array_sum($datas);
  $res=0;

  foreach( $datas as $key => $nb_user){
    $res+=(2*($key+1)-$ctTable-1)*$nb_user;
  }
  return $res/($ctTable*$somme);
}

function getNews($source) {
  $dest = fopen($source, 'w');
  $src = "https://instances.social/api/1.0/instances/list?count=0";

  $context = stream_context_create(array(
    'http' => array(
      'method' => "GET",
      'header' => "Authorization: Bearer ".Secret_Token
    ),
  ));

  $table_tmp=json_decode(file_get_contents($src, false, $context), true);
  $result=json_encode($table_tmp['instances']);
  file_put_contents($source,$result);
}

function percent($a,$b,$r=0) {
  return round($a*100/$b,$r);
}
