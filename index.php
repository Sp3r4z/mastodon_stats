<?php
require_once('functions.php');
echo custom_header('Some Mastodon informations');
?>
    <h1>Statistics and evolutions of Mastodon instances</h1>
    <h2>My personal pages</h2>
    <ul>
      <li><a href="general">General stats</a></li>
      <li><a href="accurate_user_count">More accurate users count</a></li>
      <li><a href="all_stats">Accurate all datas statistics</a></li>
      <li><a href="instances_dbd">Instances evolution day by day</a></li>
      <li><a href="lasts">New instances of the day</a></li>
      <li><a href="gone">Gone instances of the day</a></li>
      <li><a href="not_connected">Instances without any fediverse connections</a></li>
      <li>Instances evolution
        <ul>
          <li><a href="instances">Daily</a></li>
          <li><a href="instances_weekly">Weekly</a></li>
          <li><a href="instances_biweekly">Last two weeks</a></li>
          <li><a href="instances_monthly">Last month</a></li>
          <li><a href="instances_bimonthly">Last two months</a></li>
          <li><a href="instances_sixmonths">Last six months</a></li>
        </ul>
      </li>
      <li><a href="ips">Instance counts by IP address</a></li>
      <li><a href="countries">Instance counts by country</a></li>
      <li><a href="providers">Instance counts by provider</a></li>
      <li><a href="https">Instance counts by HTTPS rank</a></li>
      <li><a href="versions">Instance counts by version</a></li>
    </ul>
    <h2>Instance hosting locations by <a href="https://miaou.drycat.fr/@Dryusdan">@Dryusdan</a> (update once a day)</h2>
    <a href="https://masto.dryusdan.fr/locations.json">JSON instances locations file</a>
    <h2>Source code</h2>
    <a href="https://framagit.org/Sp3r4z/mastodon_stats/tree/master">Project on Framagit</a><br>
    <a href="https://mastodon.xyz/@Sp3r4z" rel="me">@Sp3r4z on Mastodon</a>
  </div>
  <?=custom_footer()?>
</body>
</html>
