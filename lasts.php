<?php
require_once('functions.php');
echo custom_header('New instances of the day');
?>
<h1>New Mastodon instances added since yesterday based on: <a href='https://instances.social/'>instances.social</a></h1>
<?php
require 'database/ini.php';
function getRank($s) {
  if($s<20) return 'F';
  else if($s<35) return 'E';
  else if($s<50) return 'D';
  else if($s<65) return 'C';
  else if($s<80) return 'B';
  else if($s<100) return 'A';
  else return 'A+';
}
$db=new Database($db_type,$db_host,$db_name,$db_user,$db_pwd);
$sql='SELECT distinct date FROM mastodon ORDER BY date DESC LIMIT 2';
$result=$db->select($sql);
$cronTime=$result[0]->date;
$pastTime=$result[1]->date;
echo "Last refresh: ".date("d M Y G:i:s T",$cronTime)." (refresh everyday at: 1:35am Europe/Paris)";
$sql="SELECT * FROM mastodon WHERE date=? ORDER BY users DESC";
$data=[$cronTime];
$resultToday=$db->select($sql,$data);
$sql="SELECT name FROM mastodon WHERE date=? GROUP BY name ORDER BY name ASC";
$data=[$pastTime];
$resultPast=$db->select($sql,$data);
$tmpArray=array();
foreach ($resultPast as $key => $row) {
  $tmpArray[$row->name]=1;
}
$finalArray=array();
foreach ($resultToday as $key => $row) {
  $tmp=array_key_exists($row->name,$tmpArray);
  if(!$tmp) {
    array_push($finalArray,$row);
  }
}

$db = new Database("sqlite",__DIR__."/database.db");
$names = array_column(json_decode(json_encode($finalArray),true),'name');
$list_names = (count($names)>1)? implode('", "',$names): $names[0];
$locations = $db->select('SELECT name, country FROM locations WHERE name IN ("'.$list_names.'")');

foreach ($finalArray as $key => $row) {
  $open[$key] = $row->openRegistrations;
}
?>
<h2>Quick information</h2>
There is <strong><?= count($finalArray)?></strong> new instances<br>
With <strong><?= array_sum($open)?></strong> open instances & <strong><?= count($open)-array_sum($open)?></strong> closed instances<br><br>
<table class='table table-striped sortable'>
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th><i class='icon-users' aria-hidden='true' title='Users count'></i><span class='sr-only'>Number of users</span></th>
      <th><i class='icon-globe' aria-hidden='true' title='Country'></i><span class='sr-only'>Country of instance</span></th>
      <th>HTTPS</th>
      <th>IPv6</th>
      <th>Open</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach ($locations as $keyL => $rowL) {
      $tableLocations[$rowL->name]=$rowL->country;
    }
    $i=0;
    foreach ($finalArray as $key => $row) {
      $i++;
      echo '<tr>
      <td>'.$i.'</td>
      <td><a href="https://'.$row->name.'">'.$row->name.'</a></td>';
      echo '<td>'.$row->users.'</td>';
      echo (isset($tableLocations[$row->name]))?'<td>'.$tableLocations[$row->name].'</td>':'<td>Unknown</td>';
      echo ($row->https_score>=80) ? "<td class='success' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>" : (($row->https_score>65) ? "<td class='warning' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>" : "<td class='danger' data-value='".$row->https_score."'>".getRank($row->https_score)."</td>");
      echo ($row->ipv6)?"<td class='success'>YES</td>":"<td class='danger'>NO</td>";
      echo ($row->openRegistrations)?"<td class='success'>YES</td>":"<td class='danger'>NO</td>";
      echo '</tr>';
    }
    ?>
  </tbody>
</table>
</div>
<?=custom_footer()?>
</body>
</html>
