<?php
require_once('functions.php');
echo custom_header('Number of Mastodon instance when change');
?>
<h1>Instances evolution day by day based on: <a href='https://instances.social/'>instances.social</a></h1>
<?php
require 'database/ini.php';
$db=new Database($db_type,$db_host,$db_name,$db_user,$db_pwd);
$sql='SELECT date, count(*) as nb_i, sum(users) as nb_u, sum(statuses) as nb_s FROM mastodon GROUP BY date ORDER BY date DESC';
$result=$db->select($sql);
$cronTime=$result[0]->date;
echo "Last refresh: ".date("d M Y G:i:s T",$cronTime)." (refresh everyday at: 1:35am Europe/Paris)";
?>
<table class='table table-striped sortable'>
  <thead>
    <th>Date</th>
    <th>Instances</th>
    <th>↑</th>
    <th><i class='icon-users' aria-hidden='true' title='Users count'></i><span class='sr-only'>Number of users</span></th>
    <th>↑</th>
    <th><i class='icon-sticky-note-o' aria-hidden='true' title='Toots count'></i><span class='sr-only'>Number of toots</span></th>
    <th>↑</th>
  </thead>
  <tbody>
    <?php
    foreach ($result as $key => $val) {
      echo '<tr>
      <td>'.date('Y-m-d',$val->date).'</td>
      <td data-value="'.$val->nb_i.'">'.number_format($val->nb_i, 0, ',', ' ').'</td>';
      $insPlus=$val->nb_i-$result[$key+1]->nb_i;
      echo ($insPlus>0)?"<td class='success' data-value='".$insPlus."'>+".number_format($insPlus,0,',',' ')."</td>":(($insPlus==0)?"<td class='warning' data-value='".$insPlus."'>".number_format($insPlus,0,',',' ')."</td>":"<td class='danger' data-value='".$insPlus."'>".number_format($insPlus,0,',',' ')."</td>");
      echo '<td>'.number_format($val->nb_u, 0, ',', ' ').'</td>';
      $usersPlus=$val->nb_u-$result[$key+1]->nb_u;
      echo ($usersPlus>0)?"<td class='success' data-value='".$usersPlus."'>+".number_format($usersPlus,0,',',' ')."</td>":(($usersPlus==0)?"<td class='warning' data-value='".$usersPlus."'>".number_format($usersPlus,0,',',' ')."</td>":"<td class='danger' data-value='".$usersPlus."'>".number_format($usersPlus,0,',',' ')."</td>");
      echo '<td>'.number_format($val->nb_s, 0, ',', ' ').'</td>';
      $statPlus=$val->nb_s-$result[$key+1]->nb_s;
      echo ($statPlus>0)?"<td class='success' data-value='".$statPlus."'>+".number_format($statPlus,0,',',' ')."</td>":(($statPlus==0)?"<td class='warning' data-value='".$statPlus."'>".number_format($statPlus,0,',',' ')."</td>":"<td class='danger' data-value='".$statPlus."'>".number_format($statPlus,0,',',' ')."</td>");
      echo '</tr>';
    }
    ?>
  </tbody>
</table>
</div>
<?=custom_footer()?>
</body>
</html>
