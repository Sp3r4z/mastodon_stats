<?php
require_once('functions.php');
echo custom_header('Gone Mastodon instances of the day');
?>
<h1>Gone Mastodon instances of the day based on: <a href='https://instances.social/'>instances.social</a></h1>
<?php
require 'database/ini.php';
$db=new Database($db_type,$db_host,$db_name,$db_user,$db_pwd);
$sql='SELECT distinct date FROM mastodon ORDER BY date DESC LIMIT 2';
$result=$db->select($sql);
$cronTime=$result[0]->date;
$pastTime=$result[1]->date;
echo "Last refresh: ".date("d M Y G:i:s T",$cronTime)." (refresh everyday at: 1:35am Europe/Paris)";
$sql="SELECT DISTINCT name FROM mastodon WHERE date=? ORDER BY name ASC";
$data=[$cronTime];
$resultToday=$db->select($sql,$data);
$sql="SELECT name, users, statuses FROM mastodon WHERE date=? ORDER BY users DESC";
$data=[$pastTime];
$resultPast=$db->select($sql,$data);
$tmpArray=array();
foreach ($resultToday as $key => $row) {
  $tmpArray[$row->name]=1;
}
$finalArray=array();
$totalUsers=0;
$totalToots=0;
foreach ($resultPast as $key => $row) {
  $tmp=array_key_exists($row->name,$tmpArray);
  if(!$tmp) {
    $totalUsers+=$row->users;
    $totalToots+=$row->statuses;
    array_push($finalArray,array(
      "name"=>$row->name,
      "users"=>$row->users,
      "statuses"=>$row->statuses
    ));
  }
}
?>
<h2>Quick information</h2>
There are <strong><?= count($finalArray)?></strong> gone instances<br>
With <strong><?= number_format($totalUsers,0,',',' ')?></strong> losted users<br>
And <strong><?= number_format($totalToots,0,',',' ')?></strong> disappeared toots<br><br>
<table class='table table-striped sortable'>
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th><i class='icon-users' aria-hidden='true' title='Users count'></i><span class='sr-only'>Number of users</span></th>
      <th><i class="icon-sticky-note-o" aria-hidden='true' title='Toots count'></i><span class='sr-only'>Number of toots</span></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach ($finalArray as $key => $row) {
      $i++;
      echo '<tr>
      <td>'.$i.'</td>
      <td><a href="https://'.$row['name'].'">'.$row['name'].'</a></td>';
      echo '<td data-value="'.$row['users'].'">'.number_format($row['users'],0,',',' ').'</td>';
      echo '<td data-value="'.$row['statuses'].'">'.number_format($row['statuses'],0,',',' ').'</td>';
      echo '</tr>';
    }
    ?>
  </tbody>
</table>
</div>
<?=custom_footer()?>
</body>
</html>
