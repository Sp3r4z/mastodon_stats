<?php
require_once('functions.php');

$providerSearch=(isset($_REQUEST['hosting']))? $_REQUEST['hosting'] : "NONE";

echo custom_header('Provider of instances - '.$providerSearch);

require_once 'database/ini.php';

if($providerSearch=='Unknown') $where = " WHERE `asn` = 'Unknown'";
else if($providerSearch != 'NONE') $where = " WHERE `asn` = '$providerSearch'";
else $where = '';

$db = new Database("sqlite",__DIR__."/database.db");
$providers = $db->select('SELECT name, timestamp from locations '.$where.' ORDER BY name');

$sourceTime=date('d M Y G:i:s T',$providers[0]->timestamp);

?>

<h1>Mastodon instance hosted by: <?= $providerSearch ?></h1>
<h2>Hostings provide by <a href="https://miaou.drycat.fr/@Dryusdan">@Dryusdan</a></h2>

Last refresh: <?=$sourceTime?> (refresh everyday)
<h2>Quick information</h2>
<h3>There are <strong><?= count($providers) ?></strong> instances in <?= $providerSearch ?><br></h3>

<?php
foreach ($providers as $key => $row) {
  echo "<a href='https://".$row->name."/'>".$row->name."</a></br>";
}
?>
</div>
<?=custom_footer()?>
</body>
</html>
