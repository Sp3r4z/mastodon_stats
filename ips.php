<?php
require_once('functions.php');
echo custom_header('IP of instances');

require_once 'database/ini.php';

$db = new Database("sqlite",__DIR__."/database.db");
$ips = $db->select('SELECT ip, `as`, asn, timestamp, count(*) as ct FROM locations GROUP BY ip ORDER BY ct DESC, ip DESC');

$sourceTime=date('d M Y G:i:s T',$ips[0]->timestamp);

$countTotal = 0;
foreach ($ips as $key => $value) {
  $countTotal += $value->ct;
}
?>

<h1>Mastodon instances based on: <a href='https://instances.social/'>instances.social</a></h1>
<h2>IPs provide by <a href="https://miaou.drycat.fr/@Dryusdan">@Dryusdan</a></h2>
Last refresh: <?=$sourceTime?> (refresh once a day)
<h2>Quick information</h2>
There are <strong><?= $countTotal ?></strong> instances<br>
Powered by <strong><?= count($ips) ?></strong> different IP</br></br>

<?php
foreach ($ips as $key => $row) {
  echo '<a href="ips_list?ip='.$row->ip.'" title="'.$row->as.' - '.$row->asn.'">'.$row->ip.'</a> : <strong>'.$row->ct.'</strong> ('.round(100*$row->ct/$countTotal,2).'%)</br>';
}
?>

</div>
<?=custom_footer()?>
</body>
</html>
