<?php
$versionSearch=(isset($_REQUEST['version']))? $_REQUEST['version'] : "NONE";
require_once('functions.php');
echo custom_header('Version of instances - '.$versionSearch);

require_once 'database/ini.php';

if($versionSearch=='Unknown') $where = " AND version IS NULL";
else if($versionSearch != 'NONE') $where = " AND version = '$versionSearch'";
else $where = '';

$db = new Database("sqlite",__DIR__."/database.db");
$versions = $db->select('SELECT version, name, timestamp from instances where users > 0'.$where.' ORDER BY name');
$sourceTime = $versions[0]->timestamp;

echo "Last refresh: ".date("d M Y G:i:s T",$sourceTime)." (refresh everyday)";
?>

<h1>Mastodon instance hosted in: <?= $versionSearch ?></h1>

<h2>Quick information</h2>
<h3>There are <strong><?= count($versions) ?></strong> instances on version <?= $versionSearch ?><br></h3>

<?php
foreach ($versions as $key => $row) {
  echo "<a href='https://".$row->name."/'>".$row->name."</a></br>";
}
?>

</div>

<?=custom_footer()?>

</body>
</html>
