const body = document.body,
navbar = document.getElementById('navbar'),
darkTheme = document.getElementById('darkTheme'),
toggleDarkTheme = _ => {

  if(localStorage.getItem("darkTheme") === "true") {
    localStorage.setItem("darkTheme", "false");
    changeRootColors('light')
    changeButton('light')
  }
  else {
    localStorage.setItem("darkTheme", "true");
    changeRootColors()
    changeButton()
  }
},
changeRootColors = (style = "dark") => {
  const docStyle = document.documentElement.style

  if(style == "dark") {
    docStyle.setProperty('--main-light', '#333');
    docStyle.setProperty('--main-bgc', '#202020');
    docStyle.setProperty('--table-font-color', '#000');
    docStyle.setProperty('--success', '#97b68c');
    docStyle.setProperty('--danger', '#b99393');
    docStyle.setProperty('--font-color','#d9d9d9');
    docStyle.setProperty('--main-link','#82cbff');
  }
  else {
    docStyle.removeProperty('--main-light');
    docStyle.removeProperty('--main-bgc');
    docStyle.removeProperty('--table-font-color');
    docStyle.removeProperty('--success');
    docStyle.removeProperty('--danger');
    docStyle.removeProperty('--font-color');
    docStyle.removeProperty('--main-link');
  }
},
changeButton = (style = "dark") => {
  if(style == "dark") {
    darkTheme.classList.remove('btn-outline-dark')
    darkTheme.classList.add('btn-outline-light')
    darkTheme.innerHTML = "Light theme"
  }
  else {
    darkTheme.classList.add('btn-outline-dark')
    darkTheme.classList.remove('btn-outline-light')
    darkTheme.innerHTML = "Dark theme"
  }
}

if (localStorage.getItem("darkTheme") === null) {
  localStorage.setItem("darkTheme", "true")
}

if (localStorage.getItem("darkTheme") === "true") {
  darkTheme.innerHTML = "Light theme"
  changeRootColors()
}
else {
  changeButton("light");
}

darkTheme.addEventListener('click', _ => {
  toggleDarkTheme()
});
