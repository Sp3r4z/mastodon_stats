const tablePerso = document.getElementsByClassName('table-perso'),
tableShow = document.getElementsByClassName('tableShow')

if(tablePerso.length > 0) {
  [].forEach.call(tablePerso,e=>{
    e.classList.add('d-none');
  })
  let first = document.getElementById('premier')
  first.classList.replace('d-none','d-table')
  first.previousSibling.classList.add('addArrow')
}

if(tableShow.length > 0) {
  [].forEach.call(tableShow,e=>{
    e.addEventListener('click',()=>{
      [].forEach.call(tablePerso,tp=>{
        tp.classList.replace('d-table','d-none');
      });
      [].forEach.call(tableShow,ts=>{
        ts.classList.remove('addArrow')
      })
      e.classList.add('addArrow')
      e.nextSibling.classList.replace('d-none','d-table');
    })
  })
}
