<?php
require_once('functions.php');

$ipSearch=(isset($_REQUEST['ip']))? $_REQUEST['ip'] : "NONE";

echo custom_header('Instances list by IP - '.$ipSearch);

require_once 'database/ini.php';

if($ipSearch != 'NONE') $where = " WHERE ip = '$ipSearch'";
else $where = '';

$db = new Database("sqlite",__DIR__."/database.db");
$ips = $db->select('SELECT name, timestamp from locations '.$where.' ORDER BY name');

$sourceTime=date('d M Y G:i:s T',$ips[0]->timestamp);

?>

<h1>Mastodon instances based on: <a href='https://instances.social/'>instances.social</a></h1>
<h2>IPs provide by <a href="https://miaou.drycat.fr/@Dryusdan">@Dryusdan</a></h2>
Last refresh: <?=$sourceTime?> (refresh once a day)
<h2>Quick information</h2>
<h3>There are <strong><?= count($ips) ?></strong> instances on ip: <?= $ipSearch ?><br></h3>

<?php
foreach ($ips as $key => $row) {
  echo "<a href='https://".$row->name."/'>".$row->name."</a></br>";
}
?>

</div>
<?=custom_footer()?>

</body>
</html>
