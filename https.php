<?php
require_once('functions.php');
echo custom_header('HTTPS scoring of instances');

require_once 'database/ini.php';

$db = new Database("sqlite",__DIR__."/database.db");
$https = $db->select('SELECT timestamp, https_rank, count(*) as ct from instances where users > 0 GROUP BY https_rank ORDER BY ct DESC');
$sourceTime = $https[0]->timestamp;

$countTotal = 0;
foreach ($https as $key => $value) {
  $countTotal += $value->ct;
}

echo "Last refresh: ".date("d M Y G:i:s T",$sourceTime)." (refresh everyday)";
?>

<h1>Mastodon instance HTTPS scoring based on: <a href='https://instances.social/'>instances.social</a></h1>
<h2>Quick information</h2>
There are <strong><?= $countTotal ?></strong> instances<br>
Within <strong><?= count($https) ?></strong> HTTPS ranks</br></br>
<?php
foreach ($https as $key => $row) {
  ( is_null($row->https_rank) ) ? $rank="Unknown" : $rank=$row->https_rank;
  ($row->https_rank=="A+")?$url="A%2B":$url=$rank;

  echo "<a href='https_list?rank=".$url."'>".$rank."</a>: <strong>".$row->ct."</strong> (".round(100*$row->ct/$countTotal,2)."%)</br>";
}
?>
</div>
<?=custom_footer()?>
</body>
</html>
