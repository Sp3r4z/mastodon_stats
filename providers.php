<?php
require_once('functions.php');
echo custom_header('Provider of instances');

require_once 'database/ini.php';

$db = new Database("sqlite",__DIR__."/database.db");

$providers = $db->select('SELECT `as`, asn, count(*) AS ct, timestamp from locations GROUP BY `asn` ORDER BY ct DESC, `as` DESC');

$sourceTime=date('d M Y G:i:s T',$providers[0]->timestamp);

$countTotal = 0;
foreach ($providers as $key => $value) {
  $countTotal += $value->ct;
}
?>

<h1>Mastodon instances based on: <a href='https://instances.social/'>instances.social</a></h1>
<h2>Hostings provide by <a href="https://miaou.drycat.fr/@Dryusdan">@Dryusdan</a></h2>
Last refresh: <?=$sourceTime?> (refresh everyday)

<h2>Quick information</h2>
There are <strong><?= $countTotal ?></strong> instances<br>
Hosted by <strong><?= count($providers) ?></strong> providers</br></br>
<?php
foreach ($providers as $key => $row) {
  echo '<a href="providers_list?hosting='.$row->asn.'" title="'.$row->asn.'">'.$row->as.'</a> : <strong>'.$row->ct.'</strong> ('.round($row->ct*100/$countTotal,2).'%)</br>';
}
?>
</div>
<?=custom_footer()?>
</body>
</html>
