<?php
require_once('functions.php');
echo custom_header('More accurate user count');

require_once 'database/ini.php';

$db = new Database("sqlite",__DIR__."/database.db");

$instances = $db->select('SELECT name, active_users, users, timestamp, (active_users*100)/users AS percent from instances WHERE users != 0 AND active_users != 0 ORDER BY name');

$sourceTime=date('d M Y G:i:s T',$instances[0]->timestamp);

$userTotal = 0;
$activeTotal = 0;
foreach ($instances as $key => $value) {
  $userTotal += $value->users;
  $activeTotal += $value->active_users;
}

?>

<h1>Mastodon instances based on: <a href='https://instances.social/'>instances.social</a></h1>
Last refresh: <?=$sourceTime?> (refresh everyday)

<h2>Quick information</h2>
There are <strong><?= number_format(count($instances),0,',',' ') ?></strong> listed instancess<br>
With <strong><?= number_format($activeTotal,0,',',' ') ?></strong> active users (logged in the week)<br>
And <strong><?= number_format($userTotal,0,',',' ') ?></strong> registered users</br></br>

<table class='table table-striped sortable'>
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Number of users</th>
      <th>Number of active users</th>
      <th>% of active users</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=0;
    foreach ($instances as $key => $row) {
      $i++;
      echo '<tr>
      <td>'.$i.'</td>
      <td><a href="profile?uri='.base64_encode($row->name).'">'.$row->name.'</a></td>';
      echo '<td data-value="'.$row->users.'">'.number_format($row->users,0,',',' ').'</td>';
      echo '<td data-value="'.$row->active_users.'">'.number_format($row->active_users,0,',',' ').'</td>';
      echo '<td data-value="'.$row->percent.'">'.number_format($row->percent,0,',',' ').'%</td>';
      echo '</tr>';
    }
    ?>
  </tbody>
</table>
</div>
<?=custom_footer()?>
</body>
</html>
