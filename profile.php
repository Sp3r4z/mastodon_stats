<?php
require 'database/ini.php';

$db = new Database("sqlite",__DIR__."/database.db");
$instance = base64_decode($_GET['uri']);

$sql='SELECT * FROM locations JOIN instances ON locations.name = instances.name WHERE instances.name = ?';
$location=$db->select($sql,[$instance],true);

if(!$location) {
	header('Location: http://sp3r4z.fr/mastodon/');
	die();
}
else {
	$db=new Database($db_type,$db_host,$db_name,$db_user,$db_pwd);
	$sql='SELECT * FROM mastodon WHERE name="'.$instance.'" ORDER BY date DESC';
	$result=$db->select($sql);
	$rl=count($result);
	$users=$result[0]->users-$result[$rl-1]->users;
	$statuses=$result[0]->statuses-$result[$rl-1]->statuses;

	require_once('functions.php');
	echo custom_header('All datas about : '.$instance);
	?>
	<h1>Informations about <a href="https://<?=$instance?>/"><?=$instance?></a><br>based on: <a href='https://instances.social/'>instances.social</a></h1>

	<h2>Quick information</h2>

	From <strong><?=date("Y-m-d",$result[$rl-1]->date)?></strong> to <strong><?=date("Y-m-d",$result[0]->date)?></strong><br>

	There are <strong><?= number_format($users,0,',',' ') ?></strong> new users<br>
	There are <strong><?= number_format($statuses,0,',',' ') ?></strong> new toots<br><br>

	Actually <strong><?=($result[0]->openRegistrations)?"opened":"closed"?></strong> to registrations<br>
	Hosted in <strong><?=$location->country?></strong><br>
	by <strong><?=$location->as?></strong><br>
	on <strong><?=$location->ip?></strong><br>
	Running v<strong><?=$location->version?></strong><br>

	<br>

	<table class='table table-striped sortable'>
		<thead>
			<tr>
				<th>Date</th>
				<th title="users"><i class="icon-users" aria-hidden='true'></i></th>
				<th>↑</th>
				<th title="toots"><i class="icon-sticky-note-o" aria-hidden='true'></i></th>
				<th>↑</th>
				<th title="toots per user"><i class="icon-sticky-note-o" aria-hidden='true'></i> / <i class="icon-user" aria-hidden='true'></i></th>
				<th>↑</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$i=0;
			foreach ($result as $key => $row) {
				$i++;
				if($i!=$rl) $y=$result[$i];
				echo '<tr>
				<td data-value="'.$row->date.'">'.date("Y-m-d",$row->date).'</td>
				<td data-value="'.$row->users.'">'.number_format($row->users,0,',',' ').'</td>';
				$tmp=($row->users-$y->users);
				echo ($i!=$rl)?(($tmp>=1)?'<td class="success" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>':(($tmp<0)?'<td class="danger" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>':'<td class="warning" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>')):'<td></td>';
				echo '<td data-value='.$row->statuses.'>'.number_format($row->statuses,0,',',' ').'</td>';
				$tmp=($row->statuses-$y->statuses);
				echo ($i!=$rl)?(($tmp>=1)?'<td class="success" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>':(($tmp<0)?'<td class="danger" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>':'<td class="warning" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>')):'<td></td>';
				echo '<td>'.number_format(round($row->statuses/$row->users),0,',',' ').'</td>';
				$tmp=round($row->statuses/$row->users)-round($y->statuses/$y->users);
				echo ($i!=$rl)?(($tmp>=1)?'<td class="success" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>':(($tmp<0)?'<td class="danger" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>':'<td class="warning" data-value="'.$tmp.'">'.number_format($tmp,0,',',' ').'</td>')):'<td></td>';
				echo '</tr>';
			}
			?>
		</tbody>
	</table>
</div>
<?=custom_footer()?>
</body>
</html>

<?php
}
?>
